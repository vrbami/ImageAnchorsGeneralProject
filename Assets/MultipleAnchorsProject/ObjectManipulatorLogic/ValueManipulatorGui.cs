﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace sense3d.ar
{
    public class ValueManipulatorGui : MonoBehaviour
    {
        public string caption;
        public Text captionText;
        public float deltaSmall = 0.01f;
        public float deltaLarge = 0.5f;

        public delegate void ValueChanged(float value);
        public event ValueChanged OnValueChanged;


        public MyButton[] buttons;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            captionText.text = caption;
            HandleButtonPressed();

        }

        private void HandleButtonPressed()
        {
            for (int i = 0; i < buttons.Length; i++)
            {
                if (buttons[i].buttonPressed)
                {
                    switch (i)
                    {
                        case 0: Right(); break;
                        case 1: RightShift(); break;
                        case 2: left(); break;
                        case 3: leftShift(); break;
                    }
                }
            }
        }

        public void left()
        {
            OnValueChanged?.Invoke(-deltaSmall);
        }

        public void Right()
        {
            OnValueChanged?.Invoke(deltaSmall);

        }

        public void leftShift()
        {
            OnValueChanged?.Invoke(-deltaLarge);

        }

        public void RightShift()
        {
            OnValueChanged?.Invoke(deltaLarge);

        }
    }
}