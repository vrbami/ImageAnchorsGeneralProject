﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sense3d.ar
{
    public class ObjectManipulator : MonoBehaviour
    {

        public ValueManipulatorGui xPos;
        public ValueManipulatorGui yPos;
        public ValueManipulatorGui zPos;

        public ValueManipulatorGui xRot;
        public ValueManipulatorGui yRot;
        public ValueManipulatorGui zRot;
        public ValueManipulatorGui scale;

        public Transform handledObject;
        public Camera cam;



        // Start is called before the first frame update
        void Start()
        {
            xPos.OnValueChanged += XPos_OnValueChanged;
            yPos.OnValueChanged += YPos_OnValueChanged;
            zPos.OnValueChanged += ZPos_OnValueChanged;

            scale.OnValueChanged += Scale_OnValueChanged;

            xRot.OnValueChanged += XRot_OnValueChanged;
            yRot.OnValueChanged += YRot_OnValueChanged;
            zRot.OnValueChanged += ZRot_OnValueChanged;
        }

        private void ZRot_OnValueChanged(float value)
        {
            handledObject.transform.RotateAround(handledObject.transform.position, Vector3.forward, value);
        }

        private void XRot_OnValueChanged(float value)
        {
            handledObject.transform.RotateAround(handledObject.transform.position, Vector3.right, value);
        }

        private void YRot_OnValueChanged(float value)
        {
            handledObject.transform.RotateAround(handledObject.transform.position, Vector3.up, -value);
        }

        private void Scale_OnValueChanged(float value)
        {
            handledObject.transform.localScale += Vector3.one * value;
        }

        private void ZPos_OnValueChanged(float value)
        {
            handledObject.transform.position += handledObject.transform.forward * value;
        }

        private void YPos_OnValueChanged(float value)
        {
            handledObject.transform.position += handledObject.transform.up * value;
        }

        private void XPos_OnValueChanged(float value)
        {
            handledObject.transform.position += handledObject.transform.right * value;
        }







        // Update is called once per frame
        void Update()
        {

        }
    }
}
