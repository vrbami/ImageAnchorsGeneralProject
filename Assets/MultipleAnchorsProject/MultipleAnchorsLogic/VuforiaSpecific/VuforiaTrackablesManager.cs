﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

namespace sense3d.ar
{
    public class VuforiaTrackablesManager : TrackablesManager
    {
        private StateManager sm;


        private void SaveTrackablesDefaultPositions()
        {
            var trackables = GetComponentsInChildren<TrackableBehaviour>();
            foreach (var t in trackables)
            {
                trackablesDefaultPositions.Add(
                    new SerializedTransform(GetId(t),
                    t.transform.position,
                    t.transform.rotation,
                    Vector3.one));
            }
        }





        void Awake()
        {
            sm = TrackerManager.Instance.GetStateManager();
            SaveTrackablesDefaultPositions();
        }


        private string GetId(TrackableBehaviour currentBehaviour)
        {
            if (currentBehaviour is VuMarkBehaviour)
            {
                var vumarkBehaviour = (VuMarkBehaviour)currentBehaviour;
                if (vumarkBehaviour.VuMarkTarget != null)
                {
                    return (vumarkBehaviour.VuMarkTarget.InstanceId.StringValue.Substring(0, 1));

                }
            }
            else
            {
                return currentBehaviour.TrackableName;
            }

            return "NONE";
        }

        public void TargetDetected(TrackableBehaviour trackable)
        {
            var id = GetId(trackable);
            var t = GetTrackableById(id);
            if (t == null)
            {
                t = GenerateNewTrackable(id);
                AllTrackables.Add(t);
            }

            UpdateGeneralTrackable(trackable, t);
            this.TargetDetected(t);
        }



        private void UpdateGeneralTrackable(TrackableBehaviour trackable, GeneralTrackable t)
        {
            t.statusInfo = trackable.CurrentStatus + " " + trackable.CurrentStatusInfo;
            t.t = trackable.transform;


            t.size = GetTrackableDimensions(trackable);

        }

        private Vector2 GetTrackableDimensions(TrackableBehaviour trackable)
        {
            if (trackable is ImageTargetBehaviour)
            {
                return ((ImageTargetBehaviour)(trackable)).GetSize();
            }
            if (trackable is VuMarkBehaviour)
            {
                return ((VuMarkBehaviour)(trackable)).GetSize();
            }


            return new Vector2();
        }

        public void TargetLost(TrackableBehaviour trackable)
        {
            var id = GetId(trackable);
            var t = GetTrackableById(id);
            if (t != null)
            {
                base.TargetLost(t);
            }

        }

        public void TrackingStatusChanged(TrackableBehaviour trackable)
        {
            var id = GetId(trackable);
            var t = GetTrackableById(id);
            if (t != null)
            {
                t.statusInfo = trackable.CurrentStatus + " " + trackable.CurrentStatusInfo;
                t.trackableState = trackable.CurrentStatus == TrackableBehaviour.Status.TRACKED ? GeneralTrackable.TrackableState.TRACKED : GeneralTrackable.TrackableState.NOT_TRACKED;
            }
        }

    }
}
