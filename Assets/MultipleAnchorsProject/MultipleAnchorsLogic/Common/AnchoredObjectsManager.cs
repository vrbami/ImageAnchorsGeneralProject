﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace sense3d.ar
{
    public class AnchoredObjectsManager : MonoBehaviour
    {
        public AnchoredObject anchoredObject;
        public Camera arCamera;

        public TrackablesManager trackableManager;

        public bool disableAnchorsUpdate = false;

        public Toggle toggle;





        public void ResetAllAnchors()
        {
            anchoredObject.serializedTransforms = new List<SerializedTransform>();
        }

        void Awake()
        {
            trackableManager.onTrackableDetected += OnTrackableDetected;
        }


        public void RemoveCurrentlyDetected()
        {
            anchoredObject.RemoveById(LastTrackedId());
        }


        public string CurrentlyTrackedStatus()
        {
            var currentlyTracked = trackableManager.CurrentTrackedTrackable;
            if (currentlyTracked != null)
            {
                return currentlyTracked.id + " " + currentlyTracked.trackableState;
            }
            else
            {
                return "none";
            }
        }


        public string LastTrackedId()
        {
            var lastTrackable = trackableManager.LastTrackedTrackable;
            if (lastTrackable != null)
            {
                return lastTrackable.id;
            }
            else
            {
                return "-";
            }
        }


        private void OnTrackableDetected(GeneralTrackable trackable)
        {
            print("detected " + trackable.id);
            if (anchoredObject.HasTransform(trackable.id))
            {
                if (!disableAnchorsUpdate)
                {
                    print("loading saved values from " + trackable.id);
                    UpdateAnchoredObject(trackable);
                }
            }
        }

        public void ResetCurrent()
        {
            var currentTrackable = trackableManager.LastTrackedTrackable;
            if (currentTrackable != null)
            {
                var savedValue = trackableManager.GetDefaultTrackableTransform(currentTrackable.id);
                anchoredObject.ResetCurrent(currentTrackable, savedValue);
            }
        }

        public void SaveCurrent()
        {
            var currentTrackable = trackableManager.LastTrackedTrackable;
            if (currentTrackable != null)
            {
                anchoredObject.CreateOrUpdateAnchor(currentTrackable.id, currentTrackable.t);
                print("saving  current:" + currentTrackable.id);
            }
        }



        private void UpdateAnchoredObject(GeneralTrackable trackable)
        {
            anchoredObject.LoadTransformByID(trackable.id, trackable.t);

        }

        public void LoadCurrent()
        {
            var currentTrackable = trackableManager.LastTrackedTrackable;
            if (currentTrackable != null && anchoredObject.HasTransform(currentTrackable.id))
            {
                print("loading from " + currentTrackable.id);
                UpdateAnchoredObject(currentTrackable);
            }
        }


        public string Serialize()
        {

            var serializedTransformsDatabase = new TransformsDatabase(null);
            serializedTransformsDatabase.serializedTransforms = anchoredObject.serializedTransforms.ToArray();
            var s = JsonUtility.ToJson(serializedTransformsDatabase, true);

            return s;
        }

        public void Deserialize(string s)
        {
            var serializedTransformsDatabase = JsonUtility.FromJson<TransformsDatabase>(s);
            anchoredObject.serializedTransforms = new List<SerializedTransform>(serializedTransformsDatabase.serializedTransforms);
            //LoadCurrent();
        }


        private void OnApplicationPause(bool pause)
        {

        }


        private void Update()
        {
            if (toggle.isOn)
            {
                var currentTrackable = trackableManager.CurrentTrackedTrackable;
                if (currentTrackable != null &&
                    anchoredObject.HasTransform(currentTrackable.id) &&
                    currentTrackable.trackableState == GeneralTrackable.TrackableState.TRACKED)
                {
                    //print("loading from " + currentTrackable.id);
                    HandleAutoUpdate(currentTrackable);
                }
            }



        }

        private void HandleAutoUpdate(GeneralTrackable currentTrackable)
        {
            UpdateAnchoredObject(currentTrackable);
        }








    }

    [Serializable]
    public class SerializedTransform
    {
        public string name;
        public Vector3 pos = Vector3.zero;
        public Quaternion rot = Quaternion.identity;
        public Vector3 scale = Vector3.one;

        public SerializedTransform(string name, Vector3 pos, Quaternion rot, Vector3 scale)
        {
            this.name = name;
            this.pos = pos;
            this.rot = rot;
            this.scale = scale;
        }

        public SerializedTransform()
        {
            pos = Vector3.zero;
            rot = Quaternion.identity;
            scale = Vector3.one;
        }

        internal SerializedTransform Clone()
        {
            return new SerializedTransform(name, pos, rot, scale);
        }
    }
    [Serializable]
    public class TransformsDatabase
    {
        public SerializedTransform[] serializedTransforms;

        public TransformsDatabase(SerializedTransform[] serializedTransforms)
        {
            this.serializedTransforms = serializedTransforms;
        }
    }
}
