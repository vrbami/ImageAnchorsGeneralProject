﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace sense3d.ar
{

    public class AnchoredObject : MonoBehaviour
    {
        public List<SerializedTransform> serializedTransforms = new List<SerializedTransform>();
        public List<SerializedTransform> defaultTransforms = new List<SerializedTransform>();


        private SerializedTransform savedTransform;
        private Transform transformHelper;

        private void Awake()
        {
            savedTransform = new SerializedTransform(gameObject.name, transform.position, transform.rotation, transform.localScale);
            transformHelper = new GameObject("transformHelper").transform;

        }



        public bool HasTransform(string id)
        {
            return GetTransformById(id) != null;
        }


        public SerializedTransform GetTransformById(string id)
        {
            foreach (var s in serializedTransforms)
            {
                print(s.name);
                if (s.name.CompareTo(id) == 0)
                {
                    return s;
                }
            }

            return null;
        }

        public void RemoveById(string id)
        {
            var t = GetTransformById(id);
            if (t != null)
            {
                serializedTransforms.Remove(t);
            }
        }


        private SerializedTransform GetOrCreateTransform(string id)
        {
            var t = GetTransformById(id);
            if (t == null)
            {
                t = new SerializedTransform();
                serializedTransforms.Add(t);
            }
            t.name = id;

            return t;
        }




        public void CreateOrUpdateAnchor(string id, Transform anchorTransform)
        {
            var t = GetOrCreateTransform(id);

            t.pos = anchorTransform.InverseTransformPoint(transform.position);
            t.rot = Quaternion.Inverse(anchorTransform.rotation) * transform.rotation;
            t.scale = transform.localScale;
        }

        public void ResetCurrent(GeneralTrackable currentTrackable, SerializedTransform sT)
        {
            var trackableId = sT.name;

            transform.position = savedTransform.pos;
            transform.rotation = savedTransform.rot;
            transform.localScale = savedTransform.scale;

            transformHelper.position = sT.pos;
            transformHelper.rotation = sT.rot;
            transformHelper.localScale = sT.scale;
            transformHelper.gameObject.name = sT.name;

            CreateOrUpdateAnchor(trackableId, transformHelper);
            LoadTransformByID(trackableId, currentTrackable.t);

        }



        internal void LoadTransformByID(string id, Transform anchorTransform)
        {
            var serializedTransform = GetTransformById(id);
            transform.position = anchorTransform.TransformPoint(serializedTransform.pos);
            transform.localScale = serializedTransform.scale;
            transform.localRotation = anchorTransform.localRotation * serializedTransform.rot;
        }



    }

}






