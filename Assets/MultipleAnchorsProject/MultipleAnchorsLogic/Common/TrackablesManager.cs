﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace sense3d.ar
{
    public abstract class TrackablesManager : MonoBehaviour
    {
        [SerializeField]
        protected List<SerializedTransform> trackablesDefaultPositions = new List<SerializedTransform>();
        [SerializeField]
        private List<GeneralTrackable> allTrackables = new List<GeneralTrackable>();
        [SerializeField]
        protected GeneralTrackable lastTrackedTrackable;
        [SerializeField]
        protected GeneralTrackable currentTrackedTrackable;

        public GeneralTrackable LastTrackedTrackable { get => lastTrackedTrackable; }
        public GeneralTrackable CurrentTrackedTrackable { get => currentTrackedTrackable; }
        public List<GeneralTrackable> AllTrackables { get => allTrackables; }

        public delegate void TrackableDetected(GeneralTrackable trackable);
        public event TrackableDetected onTrackableDetected;


        public SerializedTransform GetDefaultTrackableTransform(string id)
        {
            foreach (var savedTransform in trackablesDefaultPositions)
            {
                if (savedTransform.name == id)
                {
                    return savedTransform;
                }
            }
            return null;
        }


        public GeneralTrackable GetTrackableById(string id)
        {
            GeneralTrackable trackable = null;
            foreach (var t in allTrackables)
            {
                if (t.id == id)
                {
                    trackable = t;
                    break;
                }
            }

            return trackable;
        }

        public GeneralTrackable GenerateNewTrackable(string id)
        {
            var trackable = new GeneralTrackable();
            trackable.id = id;
            return trackable;
        }

        public void TargetDetected(GeneralTrackable trackable)
        {
            lastTrackedTrackable = trackable;
            currentTrackedTrackable = trackable;
            onTrackableDetected?.Invoke(trackable);
        }

        public void TargetLost(GeneralTrackable trackable)
        {
            if (trackable == currentTrackedTrackable)
            {
                currentTrackedTrackable = null;
            }
        }

        public void Update()
        {

            foreach (var t in allTrackables)
            {
                t.Update(Time.deltaTime);
            }
        }

        public void Reset()
        {
            lastTrackedTrackable = null;
            currentTrackedTrackable = null;

            allTrackables = new List<GeneralTrackable>();
        }

        private void OnApplicationPause(bool pause)
        {
            lastTrackedTrackable = null;
            currentTrackedTrackable = null;
        }

        public bool IsAnyTracked()
        {
            foreach (var t in AllTrackables)
            {
                if (t.trackableState == GeneralTrackable.TrackableState.TRACKED)
                {
                    return true;
                }
            }

            return false;
        }
    }

    [Serializable]
    public class GeneralTrackable
    {
        public Transform t;
        public string id;
        public string statusInfo;
        public float stateTimer = 0f;
        public TrackableState trackableState;
        private TrackableState previousState;
        public Vector2 size;

        public enum TrackableState
        {
            TRACKED, NOT_TRACKED
        }

        public void Update(float deltaTime)
        {
            if (previousState != trackableState)
            {
                stateTimer = 0;
                previousState = trackableState;
            }
            else
            {
                stateTimer += deltaTime;
            }
        }


    }


}