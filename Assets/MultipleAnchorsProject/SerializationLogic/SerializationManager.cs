﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace sense3d.ar
{
    public class SerializationManager : MonoBehaviour
    {
        public AnchoredObjectsManager anchorsManager;
        private string serKey = "anchors";
        public string url = "";


        public CalibrationType currentCalibrationType = CalibrationType.NONE;

        // Start is called before the first frame update
        void Start()
        {
            WebParameters.Get.onDataReceived += DeserializeFromServer;
            Load();
        }

        public void LoadCurrent()
        {
            anchorsManager.LoadCurrent();
        }

        public void SaveCurrent()
        {
            anchorsManager.SaveCurrent();
        }

        private void SaveLocally(string s)
        {
            PlayerPrefs.SetString(serKey, s);
            PlayerPrefs.Save();
        }

        public void Save()
        {
            print("saving calibration");
            var s = anchorsManager.Serialize();
            SaveLocally(s);
            WebParameters.Get.SendDataToServer(s);
        }

        private void DeserializeFromServer(string s)
        {
            anchorsManager.Deserialize(s);
            SaveLocally(s);
            currentCalibrationType = CalibrationType.FROM_SERVER;
        }

        public void Load()
        {
            if (PlayerPrefs.HasKey(serKey))
            {
                print("loading local calibration");
                var s = PlayerPrefs.GetString(serKey);
                anchorsManager.Deserialize(s);
                currentCalibrationType = CalibrationType.LOCAL;
            }
            else
            {
                print("no local calibration");
            }
        }

        public void LoadFromServer()
        {
            WebParameters.Get.url = url;
            WebParameters.Get.LoadWebData();
        }


        public void Reset()
        {
            anchorsManager.ResetAllAnchors();
        }

        public enum CalibrationType
        {
            NONE, LOCAL, FROM_SERVER
        }


    }
}
