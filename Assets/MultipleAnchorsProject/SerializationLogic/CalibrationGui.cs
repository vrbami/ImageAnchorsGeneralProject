﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace sense3d.ar
{
    public class CalibrationGui : MonoBehaviour
    {
        public AnchoredObjectsManager anchoredObjectsManager;
        public SerializationManager serializationManager;

        public Text anchorsList;





        private void SwitchAnchorsUpdate(bool b)
        {
            anchoredObjectsManager.disableAnchorsUpdate = b;
        }

        private void Update()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("last loaded " + serializationManager.currentCalibrationType);
            sb.AppendLine();
            sb.AppendLine("last result " + WebParameters.Get.lastResult);
            sb.AppendLine();
            sb.AppendLine("currently tracked " + anchoredObjectsManager.CurrentlyTrackedStatus());
            sb.AppendLine();
            sb.AppendLine("setting: ");
            sb.AppendLine(anchoredObjectsManager.LastTrackedId());
            sb.AppendLine();
            sb.AppendLine("saved anchors: ");


            for (int i = 0; i < anchoredObjectsManager.anchoredObject.serializedTransforms.Count; i++)
            {
                var a = anchoredObjectsManager.anchoredObject.serializedTransforms[i];
                sb.AppendLine(a.name);
            }
            anchorsList.text = sb.ToString();
        }


    }
}
