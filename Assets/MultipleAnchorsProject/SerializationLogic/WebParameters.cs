﻿using CI.HttpClient;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Vuforia;

namespace sense3d.ar
{
    public class WebParameters : MonoBehaviour
    {
        public static WebParameters Get { get; private set; }
        public string currentData { get; private set; }
        public string url = "http://data.3dsense.org/IPN/calibration.php";

        public delegate void DataReceived(string s);
        public event DataReceived onDataReceived;

        public string lastResult;


        void Awake()
        {
            Get = this;
            currentData = "";
            //StartCoroutine(LoadData(1));

            // StartCoroutine(SaveData());
        }

        public void LoadWebData()
        {
            StartCoroutine(LoadData(0));
        }

        IEnumerator LoadData(float delay)
        {
            yield return new WaitForSeconds(delay);

            HttpClient client = new HttpClient();

            client.Get(new System.Uri(url), HttpCompletionOption.AllResponseContent, (r) =>
            {
                if (r.IsSuccessStatusCode)
                {
                    string data = r.ReadAsString();
                    Debug.Log("loaded data: " + data);
                    currentData = data;
                    onDataReceived?.Invoke(currentData);
                }
                else
                {
                    Debug.Log("Calibration data failed to download " + r.StatusCode + ": " + r.ReasonPhrase);
                }
            });

            print("calibration data " + currentData);
        }

        public void SendDataToServer(string data)
        {
            currentData = data;
            StartCoroutine(SaveData());
        }

        private IEnumerator SaveData()
        {
            yield return new WaitForSeconds(0.1f);

            var jsonData = currentData;

            HttpClient client = new HttpClient();

            IHttpContent content = new StringContent(jsonData, System.Text.Encoding.UTF8, "application/json");

            string result = "";
            client.Post(new System.Uri(url), content, HttpCompletionOption.AllResponseContent, (r) =>
            {
                if (r.IsSuccessStatusCode)
                {
                    Debug.Log("Done - " + r.ReadAsString());
                    result = "Form upload complete!";
                    lastResult = result;
                }
                else
                {
                    Debug.Log("Calibration data failed to upload " + r.StatusCode + ": " + r.ReasonPhrase);
                    result = "Error " + r.StatusCode + ": " + r.ReasonPhrase;
                    lastResult = result;
                }
            });
        }


    }
}
