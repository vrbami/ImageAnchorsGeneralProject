﻿using System.IO;
using CI.HttpClient;
using UnityEngine;
using UnityEngine.UI;

public class ExampleSceneManagerController : MonoBehaviour
{
    public Text LeftText;
    public Text RightText;
    public Slider ProgressSlider;

    public void Upload()
    {
        HttpClient client = new HttpClient();

        //ByteArrayContent content = new ByteArrayContent(buffer, "application/bytes");
        IHttpContent content = new StringContent("{\"ahoj\":\"cau\"}", System.Text.Encoding.UTF8, "application/json");
        ProgressSlider.value = 0;

        client.Post(new System.Uri("http://127.0.0.1:8000/calibration1.php"), content, HttpCompletionOption.AllResponseContent, (r) =>
        {
            if (r.IsSuccessStatusCode) { 
                Debug.Log("Done - "+r.ReadAsString());
            }
        }, (u) =>
        {
            LeftText.text = "Upload: " +  u.PercentageComplete.ToString() + "%";
            ProgressSlider.value = u.PercentageComplete;
        });
    }

    public void Download()
    {
        HttpClient client = new HttpClient();

        ProgressSlider.value = 100;

        client.Get(new System.Uri("http://127.0.0.1:8000/calibration1.php"), HttpCompletionOption.AllResponseContent, (r) =>
        {
            if (r.IsSuccessStatusCode)
            {
                Debug.Log(r.ReadAsString());
            }
        });
    }

    public void UploadDownload()
    {
        HttpClient client = new HttpClient();

        byte[] buffer = new byte[1000000];
        new System.Random().NextBytes(buffer);

        ByteArrayContent content = new ByteArrayContent(buffer, "application/bytes");

        ProgressSlider.value = 0;

        client.Post(new System.Uri("http://httpbin.org/post"), content, HttpCompletionOption.StreamResponseContent, (r) =>
        {
            RightText.text = "Download: " + r.PercentageComplete.ToString() + "%";
            ProgressSlider.value = 100 - r.PercentageComplete;
        }, (u) =>
        {
            LeftText.text = "Upload: " + u.PercentageComplete.ToString() + "%";
            ProgressSlider.value = u.PercentageComplete;
        });
    }

    public void Delete()
    {
        HttpClient client = new HttpClient();
        client.Delete(new System.Uri("http://httpbin.org/delete"), HttpCompletionOption.AllResponseContent, (r) =>
        {
#pragma warning disable 0219
            string responseData = r.ReadAsString();
#pragma warning restore 0219
        });
    }

    public void Get()
    {
        HttpClient client = new HttpClient();
        client.Get(new System.Uri("http://httpbin.org/get"), HttpCompletionOption.AllResponseContent, (r) =>
        {
#pragma warning disable 0219
            byte[] responseData = r.ReadAsByteArray();
#pragma warning restore 0219
        });
    }

    public void Patch()
    {
        HttpClient client = new HttpClient();

        StringContent content = new StringContent("Hello World");

        client.Patch(new System.Uri("http://httpbin.org/patch"), content, HttpCompletionOption.AllResponseContent, (r) =>
        {
#pragma warning disable 0219
            Stream responseData = r.ReadAsStream();
#pragma warning restore 0219
        });
    }

    public void Post()
    {
        HttpClient client = new HttpClient();

        StringContent content = new StringContent("Hello World");

        client.Post(new System.Uri("http://httpbin.org/post"), content, HttpCompletionOption.AllResponseContent, (r) =>
        {
#pragma warning disable 0219
            string responseData = r.ReadAsString();
#pragma warning restore 0219
        });
    }

    public void Put()
    {
        HttpClient client = new HttpClient();

        StringContent content = new StringContent("Hello World");

        client.Put(new System.Uri("http://httpbin.org/put"), content, HttpCompletionOption.AllResponseContent, (r) =>
        {
#pragma warning disable 0219
            string responseData = r.ReadAsString();
#pragma warning restore 0219
        });
    }
}